// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBx8fhF5wgw1ft5Q6RHHUnNy1zglLheQiY",
    authDomain: "fangfinances.firebaseapp.com",
    projectId: "fangfinances",
    storageBucket: "fangfinances.appspot.com",
    messagingSenderId: "405445644282",
    appId: "1:405445644282:web:c8123010932c800c619e43"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
